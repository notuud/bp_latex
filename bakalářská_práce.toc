\selectlanguage *{czech}
\contentsline {chapter}{\nonumberline Úvod}{13}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Kryptologie}{15}{chapter.1}%
\contentsline {subsubsection}{\nonumberline Historie}{15}{section*.3}%
\contentsline {section}{\numberline {1.1}Matematický základ}{16}{section.1.1}%
\contentsline {subsubsection}{\nonumberline Modulární aritmetika}{16}{section*.4}%
\contentsline {subsubsection}{\nonumberline Operace modulo --- zbytek po dělení}{16}{section*.5}%
\contentsline {subsubsection}{\nonumberline Rotování bitů}{17}{section*.6}%
\contentsline {subsubsection}{\nonumberline Operace XOR}{17}{section*.7}%
\contentsline {subsubsection}{\nonumberline Permutace}{17}{section*.8}%
\contentsline {chapter}{\numberline {2}Symetrické blokové šifry}{19}{chapter.2}%
\contentsline {section}{\numberline {2.1}DES}{21}{section.2.1}%
\contentsline {subsection}{\nonumberline Vlastnosti}{21}{section*.9}%
\contentsline {subsection}{\nonumberline Rozvrhnutí klíčů}{21}{section*.10}%
\contentsline {subsection}{\nonumberline Šifrování}{22}{section*.11}%
\contentsline {subsection}{\nonumberline Dešifrování}{23}{section*.12}%
\contentsline {section}{\numberline {2.2}RC5}{24}{section.2.2}%
\contentsline {subsection}{\nonumberline Vlastnosti}{24}{section*.13}%
\contentsline {subsection}{\nonumberline Rozvrhnutí klíčů}{24}{section*.14}%
\contentsline {subsection}{\nonumberline Šifrování}{25}{section*.15}%
\contentsline {subsection}{\nonumberline Dešifrování}{26}{section*.16}%
\contentsline {section}{\numberline {2.3}Blowfish}{27}{section.2.3}%
\contentsline {subsection}{\nonumberline Vlastnosti}{27}{section*.17}%
\contentsline {subsection}{\nonumberline Rozvrhnutí klíčů}{27}{section*.18}%
\contentsline {subsection}{\nonumberline Šifrování}{27}{section*.19}%
\contentsline {subsection}{\nonumberline Dešifrování}{28}{section*.20}%
\contentsline {section}{\numberline {2.4}IDEA}{29}{section.2.4}%
\contentsline {subsection}{\nonumberline Vlastnosti}{29}{section*.21}%
\contentsline {subsection}{\nonumberline Rozvrhnutí klíčů}{29}{section*.22}%
\contentsline {subsection}{\nonumberline Šifrování}{29}{section*.23}%
\contentsline {subsection}{\nonumberline Dešifrování}{31}{section*.24}%
\contentsline {section}{\numberline {2.5}AES}{32}{section.2.5}%
\contentsline {subsection}{\nonumberline Vlastnosti}{32}{section*.25}%
\contentsline {subsubsection}{\nonumberline SubByte operace}{32}{section*.26}%
\contentsline {subsubsection}{\nonumberline ShiftRows operace}{32}{section*.27}%
\contentsline {subsubsection}{\nonumberline MixColumns operace}{33}{section*.28}%
\contentsline {subsubsection}{\nonumberline AddRoundKey operace}{33}{section*.29}%
\contentsline {subsection}{\nonumberline Rozvrhnutí klíčů}{34}{section*.30}%
\contentsline {subsection}{\nonumberline Šifrování}{35}{section*.31}%
\contentsline {subsection}{\nonumberline Dešifrování}{35}{section*.32}%
\contentsline {chapter}{\numberline {3}Režimy blokových šifer}{37}{chapter.3}%
\contentsline {section}{\numberline {3.1}ECB}{37}{section.3.1}%
\contentsline {section}{\numberline {3.2}CBC}{38}{section.3.2}%
\contentsline {subsubsection}{\nonumberline Inicializační vektor}{39}{section*.33}%
\contentsline {section}{\numberline {3.3}CFB}{39}{section.3.3}%
\contentsline {section}{\numberline {3.4}OFB}{40}{section.3.4}%
\contentsline {section}{\numberline {3.5}CTR}{41}{section.3.5}%
\contentsline {section}{\numberline {3.6}PCBC}{43}{section.3.6}%
\contentsline {chapter}{\numberline {4}Aplikace}{45}{chapter.4}%
\contentsline {section}{\numberline {4.1}Návrh}{45}{section.4.1}%
\contentsline {section}{\numberline {4.2}Implementace}{46}{section.4.2}%
\contentsline {subsection}{\nonumberline Frontend}{47}{section*.34}%
\contentsline {subsection}{\nonumberline Backend}{47}{section*.35}%
\contentsline {subsubsection}{\nonumberline Třída globálních funkcí}{48}{section*.36}%
\contentsline {subsubsection}{\nonumberline Třídy šifer}{49}{section*.37}%
\contentsline {subsubsection}{\nonumberline Třídy režimů}{50}{section*.38}%
\contentsline {subsubsection}{\nonumberline Zpracování obrázků}{50}{section*.39}%
\contentsline {chapter}{\numberline {5}Simulace šíření chyb}{53}{chapter.5}%
\contentsline {subsection}{\nonumberline Použitá data}{53}{section*.40}%
\contentsline {subsection}{\nonumberline Popis simulace v textu}{55}{section*.41}%
\contentsline {subsubsection}{\nonumberline Umělé přidání chyby}{55}{section*.42}%
\contentsline {subsection}{\nonumberline Popis simulace v obrázku}{56}{section*.43}%
\contentsline {subsubsection}{\nonumberline Umělé přidání chyby}{56}{section*.44}%
\contentsline {subsection}{\nonumberline Celková chyba}{57}{section*.45}%
\contentsline {subsubsection}{\nonumberline Text}{57}{section*.46}%
\contentsline {subsubsection}{\nonumberline Obrázek}{57}{section*.47}%
\contentsline {chapter}{\numberline {6}Zhodnocení výsledků}{59}{chapter.6}%
\contentsline {subsection}{\nonumberline Hodnocení}{61}{section*.48}%
\contentsline {chapter}{\nonumberline Závěr}{63}{chapter*.49}%
\contentsline {chapter}{\nonumberline Slovník pojmů}{65}{chapter*.50}%
\contentsline {chapter}{\nonumberline Seznam použité literatury}{67}{chapter*.51}%
\contentsline {chapter}{\numberline {A}Obsah přiloženého CD}{71}{appendix.A}%
